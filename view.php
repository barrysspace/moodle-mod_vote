<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of vote
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_vote
 * @copyright  2012 onwards, onwards, University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID.
$voteid  = optional_param('v', 0, PARAM_INT);  // Vote instance ID - it should be named as the first character of the module.
$questionid = optional_param('q', 0, PARAM_INT); // The id of a question.
$optionid = optional_param('o', 0, PARAM_INT); // The id of an option.
$function = optional_param('f', 0, PARAM_INT); // The function that has been requested.

if ($id) {
    $cm = get_coursemodule_from_id('vote', $id, 0, false, MUST_EXIST);
    $vote = new mod_vote_renderable((int)$cm->instance);
} else if ($voteid) {
    $vote = new mod_vote_renderable($voteid);
    $cm = get_coursemodule_from_instance('vote', $vote->id, $vote->course->id, false, MUST_EXIST);
} else {
    print_error('invalidcoursemodule');
}

$context = context_module::instance($cm->id); // The context of the vote.

// Check that the user is logged in and has view rights.
require_login($vote->course, true, $cm);
require_capability('mod/vote:view', $context);

$eventdata = array(
    'context' => $context,
    'objectid' => $vote->id
);

$event = \mod_vote\event\vote_viewed::create($eventdata);
$event->trigger();

// Print the page header.
$PAGE->set_url('/mod/vote/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($vote->name));
$PAGE->set_heading(format_string($vote->course->fullname));
$PAGE->set_context($context);

$output = $PAGE->get_renderer('mod_vote');

// Mark viewed if required.
$completion = new completion_info($vote->course);

// The vote should be made active.
if ($vote->can_edit() && $function == VOTE_FUNC_ACTIVATE) {
    mod_vote_editlib::make_active($vote->id);
    $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id));
    redirect($url);
}

// The cache should be reset.
if ($vote->can_edit() && $function == VOTE_FUNC_RESET) {
    mod_vote_cachelib::clear_cache($vote->id);
    $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id));
    redirect($url);
}

// Display the vote module.
if ($vote->votestate == VOTE_STATE_ACTIVE) {
    // Create the voting form.
    $voteform = new mod_vote_form(null, array('id' => $cm->id, 'v' => $vote->id, 'vt' => $vote->votetype));

    if ($function == VOTE_FUNC_VOTE && $vote->can_submit()) { // The user has submitted a valid vote.
        if ($voteform->is_cancelled()) { // Return to the course.
            $url = new moodle_url("$CFG->wwwroot/course/view.php", array('id' => $vote->course->id));
            redirect($url);
        } else if ($votedata = $voteform->get_data()) {
            mod_vote_votelib::process_vote($votedata);
            // The user voted so update their completion.
            if ($completion->is_enabled($cm) && $vote->completionvoted) {
                $completion->update_state($cm, COMPLETION_COMPLETE);
            }
            $vote->set_voted();
        }
    }

    // User viewed the active form.
    $completion->set_module_viewed($cm);

    // The logic to work out what the user should see.
    if ($vote->results_visible()) { // Show the results.
        // Show the results.
        echo $output->render_page($vote, $cm, $output->render_poll($vote, $cm, $context));
    } else if ($vote->has_voted()) {
        // Tell the user to wait.
        echo $output->render_page($vote, $cm, get_string('already_voted', 'mod_vote',
                array('close' => userdate($vote->closedate))));
    } else {
        ob_start();
        $voteform->display();
        // Put the output into a string and then wipe and close the buffer.
        $voteformstring = ob_get_contents();
        ob_end_clean();
        // Display the voting form.
        echo $output->render_page($vote, $cm, $voteformstring);
    }
} else if ($vote->can_edit()) { // In editing mode and the user is a vote editor.
    $questionformstring = '';
    $optionformstring = '';

    if ($questionid) { // We have done something with questions.
        // Get the question from the database.
        $question = $DB->get_record('vote_question', array('id' => $questionid), '*', MUST_EXIST);

        // Create a question form.
        $questionform = new mod_vote_question_form(null, array('id' => $cm->id, 'v' => $vote->id, 'q' => $question->id));
        $questionform->set_data($question);

        if ($optionid) { // We have done something with options.
            // Get the option from the database.
            $option = $DB->get_record('vote_options', array('id' => $optionid), '*', MUST_EXIST);
            // Create an option form.
            $optionform = new mod_vote_option_form(null, array('id' => $cm->id, 'v' => $vote->id,
                'q' => $questionid, 'o' => $option->id));
            $optionform->set_data($option);

            if ($function == VOTE_FUNC_OPTION_DELETE) {
                mod_vote_editlib::delete_option($optionid);
                // Redirect to prevent possible error on refresh.
                $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id));
                redirect($url);
            }
        } else {
            $optionform = new mod_vote_option_form(null, array('id' => $cm->id, 'v' => $vote->id, 'q' => $questionid));
        }

        if ($optionform->is_cancelled()) { // Redirect to a question page.
            $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id, 'q' => $questionid));
            redirect($url);
        } else if ($submittedoption = $optionform->get_data()) { // An option was submitted.
            mod_vote_editlib::process_submittedoption($submittedoption);
            // Redirect to prevent possible duplication on refresh.
            $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id));
            redirect($url);
        } else {
            // Capture the output of the next part of the code.
            ob_start();
            $optionform->display();
            // Put the output into a string and then wipe and close the buffer.
            $optionformstring = ob_get_contents();
            ob_end_clean();
        }

        if ($function == VOTE_FUNC_QUESTION_DELETE) {
            mod_vote_editlib::delete_question($questionid);
            // Redirect to prevent possible error on refresh.
            $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id));
            redirect($url);
        }
    } else {
        // Create a question form.
        $questionform = new mod_vote_question_form(null, array('id' => $cm->id, 'v' => $vote->id));
    }

    if ($questionform->is_cancelled()) { // Redirect to the default vote page.
        $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id));
        redirect($url);
    } else if ($submittedquestion = $questionform->get_data()) { // A question was submitted.
        mod_vote_editlib::process_submittedquestion($submittedquestion);
        // Redirect to prevent possible duplication on refresh.
        $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id));
        redirect($url);
    } else {
        // Capture the output of the next part of the code.
        ob_start();
        $questionform->display();
        // Put the output into a string and then wipe and close the buffer.
        $questionformstring = ob_get_contents();
        ob_end_clean();
    }

    // Choose the page to display.
    if ($optionformstring != '' && $function == VOTE_FUNC_OPTION) {
        echo $output->render_page($vote, $cm, $optionformstring);
    } else if ($questionformstring != '' && $function == VOTE_FUNC_QUESTION) {
        echo $output->render_page($vote, $cm, $questionformstring);
    } else {
        echo $output->render_page($vote, $cm, $output->render_edit_display($vote, $cm, $context));
    }
} else { // The user cannot do anything right now.
    echo $output->render_page($vote, $cm, get_string('not_active', 'mod_vote'));
}
