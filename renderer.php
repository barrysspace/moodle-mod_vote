<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Defines the renderer for the allocationform module.
 *
 * @package    mod_vote
 * @author     Neill Magill (neill.magill@nottingham.ac.uk)
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_vote_renderer extends plugin_renderer_base {
    /**
     * Generates the html to display the results of the vote.
     *
     * @global moodle_database $DB - The Moodle database connection object.
     * @global stdClasss $CFG - The moodle configuration object.
     * @param mod_vote_renderable $vote - The database record for the vote.
     * @param stdClass $cm - The course_module object for the vote.
     * @return string
     */
    public function render_poll(mod_vote_renderable &$vote, stdClass &$cm) {
        global $CFG;
        $output = '';

        $results = $vote->get_results();
        foreach ($results as $question) {
            $output .= $this->render_poll_question($question);
        }

        if ($vote->can_edit()) {
            // Link to reset the cache.
            $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id, 'f' => VOTE_FUNC_RESET));
            $output .= html_writer::tag('p', html_writer::link($url, get_string('delete_cache', 'mod_vote')));
        }

        return $output;
    }

    /**
     * Generate html output of a question's results.
     *
     * @param stdClass $question
     * @return string
     */
    protected function render_poll_question(stdClass $question) {
        $output = html_writer::tag('h3', $question->question);

        foreach ($question->options as $option) {
            $output .= $this->render_vote_bar($option->name, $option->result, $question->maxresult,
                    $option->round, $question->rounds);
        }

        return $output;
    }

    /**
     * Renders a vote result bar.
     *
     * @param type $barname - The name of the option
     * @param type $barvalue - The number of votes recieved.
     * @param type $maxvalue - The maximum votes that were recived on the question
     * @param type $barround - The round the option was eliminated in.
     * @param type $maxround - the total number of voting rounds.
     * @return string
     */
    protected function render_vote_bar($barname, $barvalue, $maxvalue, $barround, $maxround) {
        if ($maxvalue == 0) { // Stop divide by zero errors.
            $proportion = 0;
        } else {
            $proportion = $barvalue / $maxvalue;
        }

        return html_writer::tag('div',
            html_writer::tag('div', '', array('class' => 'vote_bar',
                'style' => 'width:'.(($proportion) * 100).'%')).
            html_writer::tag('div',
                html_writer::tag('span', $barname, array('style' => '')).' '.
                html_writer::tag('span', get_string('votes', 'mod_vote', array('votes' => (($barvalue > 0 ) ? $barvalue : '0')))).
                ' '.(($barround > 0 && $barround < $maxround) ? get_string('eliminated',
                        'mod_vote', array('round' => $barround)) : ''),
                array('class' => 'vote_text')), array('class' => 'vote_option'));
    }

    /**
     * Creates the text of the vote edit page.
     *
     * @global moodle_database $DB - The moodle database connector.
     * @global stdClass $CFG - The moodle configuration object.
     * @param mod_vote_renderable $vote - The record for the vote.
     * @param stdClass $cm - The course_module for the vote.
     * @param stdClass $context - The context the vote is in.
     * @return string
     */
    public function render_edit_display(mod_vote_renderable &$vote, stdClass &$cm) {
        global $CFG;
        $output = '';
        // First we will display the poll type.
        $temp = $this->render_vote_type_information($vote->votetype);

        // Display the close date.
        $temp .= $this->render_close_date_information($vote->closedate);

        $output .= html_writer::tag('div', $temp, array('class' => 'generalbox mod_introbox'));

        $questions = $vote->get_questions();
        foreach ($questions as $question) {
            $output .= html_writer::tag('div', $this->render_question($cm, $question), array('class' => 'mod_vote_question'));
        }

        // Link to add a new question.
        $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id, 'f' => VOTE_FUNC_QUESTION));
        $output .= html_writer::tag('p', html_writer::link($url, get_string('add_question', 'mod_vote')));

        // Link to make the vote active.
        $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id, 'f' => VOTE_FUNC_ACTIVATE));
        $output .= html_writer::tag('p', html_writer::link($url, get_string('activate', 'mod_vote')));

        return $output;
    }

    /**
     * Generates a string describing the vote type.
     *
     * @param int $votetype - Should be passed a constant defined in lib.php, i.e. VOTE_TYPE_POLL
     * @return string
     */
    protected function render_vote_type_information($votetype) {
        $temp = html_writer::tag('strong', get_string('type', 'mod_vote')).': ';
        switch ($votetype) {
            case VOTE_TYPE_POLL:
                $temp = html_writer::tag('p',
                        $this->output->notification($temp.get_string('type_poll', 'mod_vote').' - '.
                                get_string('type_poll_help', 'mod_vote'),
                                'notifymessage'));
                break;
            case VOTE_TYPE_VOTE:
                $temp = html_writer::tag('p',
                        $this->output->notification($temp.get_string('type_vote', 'mod_vote').' - '.
                                get_string('type_vote_help', 'mod_vote'),
                                'notifymessage'));
                break;
            case VOTE_TYPE_AV:
                $temp = html_writer::tag('p',
                        $this->output->notification($temp.get_string('type_av', 'mod_vote').' - '.
                                get_string('type_av_help', 'mod_vote'),
                                'notifymessage'));
                break;
            default:
                $temp = html_writer::tag('p',
                        $this->output->notification($temp.get_string('type_unknown', 'mod_vote'),
                                'notifyproblem'));
                break;
        }
        return $temp;
    }

    /**
     * Generate a message to display the close date, if the date is in the past show it as an error.
     *
     * @param int $closedate - UNIX timestamp for the close date.
     * @return string
     */
    protected function render_close_date_information($closedate) {
        if ($closedate < time()) { // The close date is in tha past, highlight this to the user.
            $messagetype = 'notifyproblem';
        } else { // Future date.
            $messagetype = 'notifymessage';
        }
        return $this->output->notification(html_writer::tag('strong', get_string('closedate', 'mod_vote')).
                ': '.userdate($closedate),
                $messagetype);
    }

    /**
     * Creates html to display a question with it's editing controls for a question.
     *
     * @global object $CFG
     * @param stdClass $cm - The moodle course module object.
     * @param int $questionid - The id of the question.
     * @param string $question - The text of the question.
     * @return string
     */
    protected function render_question(stdClass &$cm, $question) {
        global $CFG;

        // URL for editing the question.
        $editurl = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id,
            'f' => VOTE_FUNC_QUESTION, 'q' => $question->id));
        // URL for deleting the question.
        $deleteurl = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id,
            'f' => VOTE_FUNC_QUESTION_DELETE, 'q' => $question->id));
        $questionstring = html_writer::tag('h3', "$question->question ".
                html_writer::link($editurl, html_writer::empty_tag('img',
                        array('src' => $this->pix_url('t/edit'),
                                'alt' => get_string('edit'),
                                'title' => get_string('edit')))).' '.
                html_writer::link($deleteurl, html_writer::empty_tag('img',
                        array('src' => $this->pix_url('t/delete'),
                                'alt' => get_string('delete'),
                                'title' => get_string('delete')))));
        // Link to add a new option.
        $url = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id, 'f' => VOTE_FUNC_OPTION,
            'q' => $question->id));
        $questionstring .= html_writer::tag('p', html_writer::link($url, get_string('add_option', 'mod_vote')));
        $questionstring .= html_writer::start_tag('ul');

        foreach ($question->options as $option) {
            $questionstring .= $this->render_option($cm, $question->id, $option);
        }

        return $questionstring;
    }

    /**
     * Creates html to display an option of a question along with it's editing controls.
     *
     * @global object $CFG
     * @param stdClass $cm - The moodle course module object.
     * @param int $questionid - The id of the question the option is for.
     * @param stdClass $option - The text for the option to be displayed.
     * @return string
     */
    protected function render_option(stdClass &$cm, $questionid, stdClass $option) {
        global $CFG;

        // URL for editing the option.
        $editurl = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id,
            'f' => VOTE_FUNC_OPTION, 'q' => $questionid, 'o' => $option->id));
        // URL for deleting the option.
        $deleteurl = new moodle_url($CFG->wwwroot.'/mod/vote/view.php', array('id' => $cm->id,
            'f' => VOTE_FUNC_OPTION_DELETE, 'q' => $questionid, 'o' => $option->id));
        $questionstring = html_writer::tag('li', "$option->name ".
                html_writer::link($editurl, html_writer::empty_tag('img',
                        array('src' => $this->pix_url('t/edit'),
                            'alt' => get_string('edit'),
                            'title' => get_string('edit'))))
                .' '.
                html_writer::link($deleteurl, html_writer::empty_tag('img',
                        array('src' => $this->pix_url('t/delete'),
                            'alt' => get_string('delete'),
                            'title' => get_string('delete')))),
                array('class' => 'mod_vote_option'));

        return $questionstring;
    }

    /**
     * Generate the string for an activity that will appear on the course overview.
     * 
     * @param mod_vote_renderable $vote
     * @return string
     */
    public function render_overview(mod_vote_renderable $vote) {
        $url = new moodle_url('/mod/vote/view.php', array('id' => $vote->cm->coursemodule));
        $link = html_writer::link($url, $vote->name);
        $output = html_writer::div(get_string('overviewname', 'mod_vote', array('link' => $link)), 'name');
        $output .= html_writer::div(
                get_string('overviewmessage', 'mod_vote', array('closedate' => userdate($vote->closedate))), 'info');
        return html_writer::div($output, 'vote overview');
    }

    /**
     * Creates all the html for a page up until the point that the text should be displayed. It includes the heading for the
     * vote form, and its introduction box.
     *
     * @param mod_vote_renderable $vote
     * @param stdClass $cm - The moodle course module object.
     * @return string
     */
    public function render_header(mod_vote_renderable &$vote, stdClass &$cm) {
        $output = $this->header();
        $output .= $this->heading($vote->name);
        if ($vote->intro) { // Conditions to show the intro can change to look for own settings or whatever.
            $output .= $this->box(format_module_intro('vote', $vote, $cm->id), 'generalbox mod_introbox', 'voteintro');
        }

        return $output;
    }

    /**
     * Creates the full footer for the page.
     * @return string
     */
    public function render_footer() {
        $output = $this->footer();
        return $output;
    }

    /**
     * Creates a full page view including all headers and footers.
     *
     * @param mod_vote_renderable $vote
     * @param stdClass $cm - The moodle course module object.
     * @param type $text - The text to be displayed
     * @return string
     */
    public function render_page(mod_vote_renderable &$vote, stdClass &$cm, $text) {
        $output = $this->render_header($vote, $cm);
        $output .= html_writer::tag('div', $text, array('id' => 'vote-main-content'));
        $output .= $this->render_footer();
        return $output;
    }
}