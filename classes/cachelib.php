<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Library class of functions for caching and retriving information from the cache tables of the vote activity..
 *
 * @package    mod_vote
 * @author     Neill Magill (neill.magill@nottingham.ac.uk)
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_vote_cachelib {
    /**
     * If present and fresh it will get the cached results for the vote, if no fresh cache it will generate one.
     *
     * The cached results are assumed to be fresh if they where generated within VOTE_CACHE_TIME seconds, or they where
     * generated after the close date of the vote. They will also be considered not frech if the vote type has been changed.
     *
     * @global moodle_database $DB - The Moodle database object.
     * @param stdClass $vote - The record for the vote.
     * @return moodle_recordset - The cached results.
     */
    public static function get_cached_results(mod_vote_renderable &$vote) {
        global $DB;
        $time = time(); // Get the current datestamp, we will use it to check if the cache is still fresh.
        if ($vote->closedate > $time) { // If the closedate has not yet passed.
            $param['time'] = $time - VOTE_CACHE_TIME; // We need to check if the cache was generated in the VOTE_CACHE_TIME.
        } else { // The close date has passed.
            $param['time'] = $vote->closedate; // So we will check that the cache was generated after it.
        }
        $param['voteid'] = $vote->id;
        if ($vote->votetype == VOTE_TYPE_AV) { // An AV type should have round values greater than 0.
            $where = 'voteid = :voteid AND updatetime > :time AND round > 0';
        } else { // Other types should have round set to 0, AV will never have a round 0.
            $where = 'voteid = :voteid AND updatetime > :time AND round = 0';
        }
        $cachevalid = $DB->record_exists_select('vote_result_cache', $where, $param);

        if (!$cachevalid) { // The cache is not valid so we need to generate it.
            if ($vote->votetype == VOTE_TYPE_AV) {
                self::build_av_cache($vote, $time);
            } else {
                self::build_cache($vote, $time);
            }
        }
        $fields = 'o.id AS id, q.id AS qid, q.question, o.id AS oid, o.optionname, MAX(c.result) AS result, MAX(c.round) AS round';
        $sql = "SELECT $fields FROM {vote_question} q "
                ."JOIN {vote_options} o ON (q.id = o.questionid) "
                ."LEFT JOIN {vote_result_cache} c ON (o.id = c.optionid) "
                ."WHERE q.voteid = :voteid "
                ."GROUP BY o.id "
                ."ORDER BY q.sortorder, q.question, q.id, round DESC, result DESC, o.sortorder, o.optionname";
        return $DB->get_recordset_sql($sql, $param); // We return the recordset of the cached results.
    }

    /**
     * Builds the vote_result_cache database entries based on the vote type being alternative voting.
     *
     * @global moodle_database $DB - The Moodle database connector.
     * @param mod_vote_renderable $vote - The database record for the vote that will have it's cache built.
     * @param int $time - UNIX timestamp that will be used a the update time.
     */
    protected static function build_av_cache(mod_vote_renderable &$vote, $time) {
        // First delete any exisiting cache entries for the vote.
        self::clear_cache($vote->id);

        $questionarray = self::get_av_reponses($vote->id);

        // Build the cache.
        foreach ($questionarray as $question) { // Calculate each questions results seperatley.
            self::calculate_av_result($question, $vote, $time);
        }
    }

    /**
     * Get the responses to the vote in a form that can be used to calculate the results of an AV vote.
     *
     * @global moodle_database $DB - The Moodle database connection object.
     * @param type $voteid - id of the vote that responses should be retrieved for.
     * @return mod_vote_question[] - An array of mod_vote_question objects.
     */
    protected static function get_av_reponses($voteid) {
        global $DB;

        $param['voteid'] = $voteid;
        $fields = 'q.id, o.id AS oid, vv.userid, vv.vote';
        $sql = "SELECT $fields FROM {vote_question} q "
                ."JOIN {vote_options} o ON (q.id = o.questionid) "
                ."LEFT JOIN {vote_votes} vv ON (o.id = vv.optionid) "
                ."WHERE q.voteid = :voteid "
                ."ORDER BY q.id, vv.userid, vv.vote";

        $questionarray = array();
        $userarray = array();

        $rs = $DB->get_recordset_sql($sql, $param);

        // Build the objects we will use to build the cache.
        foreach ($rs as $result) {
            if (!isset($questionarray[$result->id])) {
                $userarray = array(); // Reset the user array.
                $questionarray[$result->id] = new mod_vote_question();
            }

            if (!isset($userarray[$result->userid])) {
                $userarray[$result->userid] = new mod_vote_user($result->userid);
            }

            $userarray[$result->userid]->add_option($result->oid);
            if (!is_null($result->userid)) {
                $questionarray[$result->id]->add_user($userarray[$result->userid]);
            }

            $questionarray[$result->id]->add_option($result->oid);
        }

        $rs->close();
        return $questionarray;
    }

    /**
     * Calculates the results of an AV vote for a single question.
     *
     * @global moodle_database $DB - The moodle database connection object
     * @param mod_vote_question $question - The question the results are being calculated for.
     * @param mod_vote_renderable $vote - The vote that the results are being calculated for.
     * @param int $time - The UNIX timestamp that the records should be shown as last updated.
     */
    protected static function calculate_av_result(mod_vote_question &$question, mod_vote_renderable &$vote, $time) {
        global $DB;

        $round = 1;
        do {
            $complete = true; // Assume that one result will get 50% of the vote.
            $lowest = array(); // Stores the information about the option with the least votes in this round.
            $lowest['option'] = array();
            $lowest['votes'] = null;
            $highest = 0; // Used to store the highest number of votes.
            $total  = 0; // Stores a tally of the votes.

            // Build the common parts of the database record.
            $record = new stdClass();
            $record->updatetime = $time;
            $record->round = $round;
            $record->voteid = $vote->id;

            $results = $question->get_results();
            foreach ($results as $option => $votes) { // Store the number of votes for each option in this round..
                $record->optionid = $option;
                $record->result = $votes;

                // Add the cached result to the database.
                $DB->insert_record('vote_result_cache', $record);
                if ($lowest['votes'] === null) { // This is the only vote we have had.
                    $lowest['votes'] = $votes;
                    $lowest['option'] = array($option);
                } else if ($votes < $lowest['votes']) { // This is the lowest vote we have had.
                    $lowest['votes'] = $votes;
                    $lowest['option'] = array($option);
                } else if ($votes === $lowest['votes']) {
                    $lowest['option'][] = $option;
                }
                if ($highest < $votes) { // This is the highest vote we have seen.
                    $highest = $votes;
                }
                $total += $votes; // Increase the total number of votes.
            }

            // Stop divide by zero errors if no one has voted.
            if ($total > 0) {
                if (($highest / $total) < 0.5) {
                    // No result got at least 50% of the votes, and all the results are not the same.
                    $optionsnotallequal = false; // Assume that all the options have the same potential;
                    $lowestoption = null;
                    $minscore = null;
                    foreach ($lowest['option'] as $lowoption) {
                        // Check each of the low options to see which has lest alternative votes.
                        $optionscore = $question->score_option($lowoption);
                        if (is_null($minscore) || $minscore > $optionscore) {
                            if (!is_null($minscore)) {
                                $optionsnotallequal |= true;
                            }
                            // It is the first low option or a worse low option.
                            $minscore = $optionscore;
                            $lowestoption = $lowoption;
                        } else if ($minscore !== $optionscore) {
                            $optionsnotallequal |= true;
                        }
                    }

                    // We do not want to continue if all the options are equal.
                    if ($optionsnotallequal || count($lowest['option']) !== count($results)) {
                        $question->remove_option($lowestoption); // Remove the lowest option before we loop again.
                        $complete = false; // We want another voting round.
                    }
                }
            }
            $round++;
        } while (!$complete); // We will keep looping until one option for this question has 50%.
    }

    /**
     * Builds the vote_result_cache database entries based on a standard vote.
     *
     * @global moodle_database $DB - The moodle database connection object.
     * @param mod_vote_renderable $vote - The record for the vote that the cache is being built for.
     * @param int $time - UNIX timestamp for the update time.
     */
    protected static function build_cache(mod_vote_renderable &$vote, $time) {
        global $DB;

        // First delete any exisiting cache entries for the vote.
        self::clear_cache($vote->id);

        // Then calculate the number of votes for each option.
        $param['voteid'] = $vote->id;
        $fields = 'o.id AS id, q.voteid, COUNT(vv.userid) AS count';
        $sql = "SELECT $fields FROM {vote_question} q "
                ."JOIN {vote_options} o ON (q.id = o.questionid) "
                ."LEFT JOIN {vote_votes} vv ON (o.id = vv.optionid) "
                ."WHERE vv.vote = 1 AND q.voteid = :voteid "
                ."GROUP BY o.id";

        $rs = $DB->get_recordset_sql($sql, $param);
        // Build the common parts of the cache entry.
        $record = new stdClass();
        $record->voteid = $vote->id;
        $record->updatetime = $time;
        $record->round = 0;
        foreach ($rs as $result) { // Build the changing parts.
            $record->optionid = $result->id;
            $record->result = $result->count;
            // Add the result to the cache.
            $DB->insert_record('vote_result_cache', $record);
        }
        $rs->close();
    }

    /**
     * Deletes the cache entry for the vote.
     *
     * @global moodle_database $DB - The Moodle database connection object.
     * @param int $voteid - The id if the vote that the cache should be cleared for.
     */
    public static function clear_cache($voteid) {
        global $DB;
        $DB->delete_records('vote_result_cache', array('voteid' => $voteid));
    }
}
