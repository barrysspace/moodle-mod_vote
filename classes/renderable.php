<?php
// This file is part of the timetable import block
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * This class holds the information for a vote.
 *
 * @package    mod_vote
 * @author     Neill Magill (neill.magill@nottingham.ac.uk)
 * @copyright  2014 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_vote_renderable {
    /** @var int $_closedate - UNIX timestamp for the close date of the vote. */
    protected $_closedate;

    /** @var bool $_closed - true when the votes close date has passed, false when it has not. */
    protected $_closed;

    /** @var stdClass $_cm - The Moodle course_module record for the vote activity. */
    protected $_cm;

    /** @var stdClass $_course - The database record for the course the vote activity is part of.   */
    protected $_course;

    /** @var int $_id - The id of the vote activity. */
    protected $_id;

    /** @var string $_intro - The description of the vote activity. */
    protected $_intro;

    /** @var int $_introformat - The format of the intro. */
    protected $_introformat;

    /** @var string $_name - The name of the vote. */
    protected $_name;

    /** @var int $_votestate - Stores if the vote is active or in editing mode. */
    protected $_votestate;

    /** @var int $_votetype - Stores if the vote is a poll, vote or AV. */
    protected $_votetype;

    /** @var bool $_completionvoted - If true then students should vote to complete the vote. */
    protected $_completionvoted;

    /** @var bool $canedit - True if the user has editing rights to the vote. */
    private $canedit;

    /** @var bool $cansubmit - True if the user is able to submit. */
    private $cansubmit;

    /**
     * @var stdClass[] $questions - Stores an array of the questions and options
     * that are part of the vote, used to render the edit page
     */
    protected $questions;

    /**
     * @var stdClass[] $results - Stores and array of questions with the results
     * for each option, the information is used to render the results.
     */
    protected $results;

    /** @var bool $hasvoted - Stores if the user has submitted a vote for the vote activity */
    private $hasvoted;

    /**
     * Create an instance of mod_vote_renderable.
     *
     * @global moodle_database $DB - The Moodle database connection object.
     * @param int|stdClass $vote The database id of the vote, or a single record returned by get_all_instances_in_courses for it.
     * @throws coding_exception
     */
    public function __construct($vote) {
        global $DB;
        
        if (is_a($vote, 'stdClass') && !empty($vote->coursemodule)) {
            // A record from get_all_instances_in_courses() was passed.
            $this->_id = $vote->id;
            $this->_name = $vote->name;
            $this->_votetype = $vote->votetype;
            $this->_closedate = $vote->closedate;
            $this->_closed = ($vote->closedate < time());
            $this->_votestate = $vote->votestate;
            $this->_intro = $vote->intro;
            $this->_introformat = $vote->introformat;
            $this->_completionvoted = $vote->completionvoted;
            // The mod_vote_renderable object needs the id to be the course_module's id,
            // get_all_instances_in_courses() sets the id as the vote activity record id
            // and stored the course_module id as coursemodule. So we overwrite id with
            // coursemodule.
            // This should fix MOODLE-1554 Error in getting context in vote_print_overview().
            $vote->id = $vote->coursemodule;
            $this->_cm = $vote;
        } else if ((string)$vote === (string)intval($vote)) { // The value of $vote is an integer.
            $voterecord = $DB->get_record('vote', array('id' => $vote),
                'id, name, course, votetype, closedate, votestate, intro, introformat, completionvoted', MUST_EXIST);
            $this->_id = $vote;
            $this->_name = $voterecord->name;
            $this->_votetype = $voterecord->votetype;
            $this->_closedate = $voterecord->closedate;
            $this->_closed = ($voterecord->closedate < time());
            $this->_votestate = $voterecord->votestate;
            $this->_intro = $voterecord->intro;
            $this->_introformat = $voterecord->introformat;
            $this->_completionvoted = $voterecord->completionvoted;

            $this->_course = get_course($voterecord->course);

            $this->_cm = get_coursemodule_from_instance('vote', $vote, $this->_course->id, false, MUST_EXIST);
        } else {
            throw new coding_exception('invalid paramter', 'An integer or a course_module record should be passed');
        }
    }

    /**
     * Magic method to return values of private and protected properties.
     *
     * @param string $name - The name of the property a value should be returned for.
     * @return mixed - Returns the value of the property, or null if it does not exist.
     */
    public function __get($name) {
        if ($name === 'questions') {
            return $this->get_questions();
        }

        if (property_exists($this, '_'.$name)) {
            return $this->{'_'.$name};
        }
        debugging('Invalid section_info property accessed! '.$name);
        return null;
    }

    /**
     * Magic method to test if a private and protected variable isset().
     *
     * @param string $name - The name of the property that should be rested.
     * @return boolean
     */
    public function __isset($name) {
        if (property_exists($this, '_'.$name)) {
            return isset($this->{'_'.$name});
        }
        return parent::__isset($name);
    }

    /**
     * Returns information about the questions and options that are part of the vote activity, including:
     * - id: the database id of the question
     * - question: the text of the question
     * - options: a stdClass[] of the options for that question
     *
     * Each option has the following information:
     * - id: the database id fo the option
     * - name: the options text
     *
     * @global moodle_database $DB - The Moodle database connection object.
     * @return stdClass[]
     */
    public function get_questions() {
        global $DB;
        if (isset($this->questions)) {
            return $this->questions;
        }

        $this->questions = array();
        // Will be used to store the questions in an associative array, so we can reference them easily.
        $tempquestions = array();

        $params['voteid'] = $this->_id;
        $fields  = 'q.id AS questionid, q.question, o.id AS optionid, o.optionname';
        $sql = "SELECT $fields FROM {vote_question} q "
                ."LEFT JOIN {vote_options} o ON (q.id = o.questionid) "
                ."WHERE q.voteid = :voteid "
                ."ORDER BY q.sortorder, q.question, o.sortorder, o.optionname";

        $records = $DB->get_recordset_sql($sql, $params);
        foreach ($records as $record) {
            if (!isset($tempquestions[$record->questionid])) {
                // We had not seen a record for this question before, so create it.
                $tempquestions[$record->questionid] = new stdClass();
                $tempquestions[$record->questionid]->id = $record->questionid;
                $tempquestions[$record->questionid]->question = $record->question;
                $tempquestions[$record->questionid]->options = array();

                // Link by reference into the object so it is stored in the correct order.
                $this->questions[] =& $tempquestions[$record->questionid];
            }

            // Create the option.
            $tempoption = new stdClass();
            $tempoption->id = $record->optionid;
            $tempoption->name = $record->optionname;
            $tempquestions[$record->questionid]->options[] = $tempoption;
        }
        $records->close();

        return $this->questions;
    }

    /**
     * Returns an array of information suitable for rendering the results each array element has the following information:
     * - id: the database id of the question
     * - question: the text of the question
     * - rounds: the number of rounds of voting that took place
     * - maxresult: the highest number of votes an option gained
     * - options: a stdClass[] of the options and their results
     *
     * The option stdClass contains:
     * - id: the database id of the option
     * - name: the text of the option
     * - result: the final number of votes for the option
     * - round: the highest round of voting the option reached
     *
     * @return stdClass[]
     */
    public function get_results() {
        if (isset($this->results)) {
            return $this->results;
        }

        $this->results = array();
        // Store the results in a temporary associative array.
        $tempresults = array();

        $results = mod_vote_cachelib::get_cached_results($this);
        foreach ($results as $result) {
            if (!isset($tempresults[$result->qid])) {
                $tempresults[$result->qid] = new stdClass();
                $tempresults[$result->qid]->id = $result->qid;
                $tempresults[$result->qid]->question = $result->question;
                $tempresults[$result->qid]->rounds = 0;
                $tempresults[$result->qid]->maxresult = 0;
                $tempresults[$result->qid]->options = array();

                // Link the object into the results array.
                $this->results[] =& $tempresults[$result->qid];
            }

            // Create the option.
            $tempoptionresult = new stdClass();
            $tempoptionresult->id = $result->oid;
            $tempoptionresult->name = $result->optionname;
            $tempoptionresult->result = $result->result;
            if ($result->result > $tempresults[$result->qid]->maxresult) {
                // We have found a higher result.
                $tempresults[$result->qid]->maxresult = $result->result;
            }
            $tempoptionresult->round = $result->round;
            if ($result->round > $tempresults[$result->qid]->rounds) {
                // We have found a higher number of rounds.
                $tempresults[$result->qid]->rounds = $result->round;
            }
            $tempresults[$result->qid]->options[] = $tempoptionresult;
        }

        return $this->results;
    }

    /**
     * Checks if the user has submitted a vote.
     *
     * @global moodle_database $DB - The Moodle database connection object.
     * @global stdClass $USER - the currently logged in user's information.
     * @return bool
     */
    public function has_voted() {
        global $DB, $USER;
        if (!isset($this->hasvoted)) {
            $this->hasvoted = $DB->record_exists('vote_votes', array('voteid' => $this->_id, 'userid' => $USER->id));
        }

        return $this->hasvoted;
    }
    
    /**
     * Store the fact that the user has voted
     */
    public function set_voted() {
        $this->hasvoted = true;
    }

    /**
     * Checks if the user can submit a vote, has not already voted and that the close date has not passed.
     *
     * @return bool
     */
    public function can_submit() {
        if (!isset($this->cansubmit)) {
            $submitcap = has_capability('mod/vote:submit', context_module::instance($this->_cm->id));
            $this->cansubmit = (!$this->has_voted() && $submitcap && !$this->_closed);
        }
        return $this->cansubmit;
    }

    /**
     * Checks if the user is able to edit the vote.
     *
     * @return bool
     */
    public function can_edit() {
        if (!isset($this->canedit)) {
            $this->canedit = has_capability('mod/vote:edit', context_module::instance($this->_cm->id));
        }
        return $this->canedit;
    }

    /**
     * Checks if the results should be shown to the user.
     *
     * @return bool
     */
    public function results_visible() {
        // Based on the vote type determins if the results should be diapalyed.
        switch ($this->_votetype) {
            case VOTE_TYPE_POLL: // With the poll we want to show the results.
                return ($this->has_voted() || $this->_closed);
            case VOTE_TYPE_VOTE:
            case VOTE_TYPE_AV:
                return $this->_closed;
        }

        return false; // Do not shoew results if the vote type is invalid.
    }
}
