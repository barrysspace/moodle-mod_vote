<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * A class that stores a users alternative voting choices.
 *
 * @package    mod_vote
 * @author     Neill Magill (neill.magill@nottingham.ac.uk)
 * @copyright  2012 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_vote_user {
    /** @var int[] $option_array - An array of the ids of the options that the user picked. */
    protected $optionarray = array();
    protected $userid;

    public function __construct($userid) {
        $this->userid = $userid;
    }

    /**
     * Adds the option as the lowest ranked option selected by the user.
     *
     * @param int $optionid - The id of the option to be added.
     */
    public function add_option($optionid) {
        $this->optionarray[] = $optionid;
    }

    /**
     * Returns the highest ranked option for the user.
     *
     * @return int|false - returns the optionid if it exsists, or false if no option is found.
     */
    public function get_choice() {
        foreach ($this->optionarray as $option) {
            return $option; // This will return the first option if the user has one.
        }
        return false; // This should only happen if the option_array is empty.
    }

    /**
     * Gives a score to an option for a choice, if they have selected it
     * give them a larger score the higher they ranked it.
     *
     * @param type $optionid - The id of the option to score.
     * @return float
     */
    public function score_option($optionid) {
        $key = array_search($optionid, $this->optionarray);
        if ($key > 1) {
            return log(10000, $key);
        } else if ($key === 1) {
            return 10000;
        } else if ($key === 0) {
            return 100000;
        } else {
            return 0;
        }
    }

    /**
     * Removes the option from the user.
     *
     * @param int $optionid - The id of the option to be removed.
     */
    public function remove_option($optionid) {
        $key = array_search($optionid, $this->optionarray); // Find the key for the option.

        if ($key !== false) { // Then we found the choice, so we need to remove it.
            unset($this->optionarray[$key]);
        }
    }
}