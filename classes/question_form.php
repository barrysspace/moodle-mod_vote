<?php
// This file is part of the vote activity
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

/**
 * The form used to edit a question
 *
 * @package    mod_vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_vote_question_form extends moodleform {
    public function definition() {
        global $DB;
        $mform =& $this->_form;

        $id = $this->_customdata['id'];
        $voteid = $this->_customdata['v'];
        if (!empty($this->_customdata['q'])) {
            $questionid = $this->_customdata['q'];
        } else {
            $questionid = 0;
        }

        $mform->addElement('header', 'general', get_string('question_form', 'mod_vote'));

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);
        $mform->setConstant('id', $id);

        $mform->addElement('hidden', 'voteid', null);
        $mform->setType('voteid', PARAM_INT);
        $mform->setConstant('voteid', $voteid);

        $mform->addElement('hidden', 'f', null);
        $mform->setType('f', PARAM_INT);
        $mform->setConstant('f', VOTE_FUNC_QUESTION);

        if ($questionid) {
            $mform->addElement('hidden', 'q', null);
            $mform->setType('q', PARAM_INT);
            $mform->setConstant('q', $questionid);
        }

        $mform->addElement('text', 'question', get_string('question', 'mod_vote'), null);
        $mform->setType('question', PARAM_TEXT);
        $mform->addRule('question', get_string('question_required', 'mod_vote'), 'required');
        $mform->addHelpButton('question', 'question', 'mod_vote');

        $weightingarray = array(1 => 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
        $mform->addElement('select', 'sortorder', get_string('weighting', 'mod_vote'), $weightingarray, null);
        $mform->setType('sortorder', PARAM_INT);
        $mform->addHelpButton('sortorder', 'weighting', 'mod_vote');
        $mform->setDefaults('sortorder', 1);

        $this->add_action_buttons();
    }
}
