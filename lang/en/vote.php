<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English strings for vote
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod
 * @subpackage vote
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Vote';
$string['modulenameplural'] = 'Votes';
$string['modulename_help'] = 'The vote module allows you to create one of the following:

* Poll - This is a simple poll: The user chooses a single option from each question and will then be shown the results immediately.
* Vote - This is a simple poll: The user chooses a single option from each question, but the results are not visible until the voting has ended.
* Alternative vote - In this system each user ranks at least one option from each question. If no option has more than 50% of the vote, then the option with the lowest number of votes will be discarded and the next options of the people who voted for it will be used instead. This will continue until one result has over 50% of the votes.
';
$string['votefieldset'] = 'Custom example fieldset';
$string['votename'] = 'Vote name';
$string['votename_help'] = 'This is the content of the help tooltip associated with the votename field. Markdown syntax is supported.';
$string['vote'] = 'vote';
$string['pluginadministration'] = 'Vote administration';
$string['pluginname'] = 'Vote';

$string['setup'] = 'Setup the vote';
$string['not_active'] = 'Voting is not open right now, please come back later.';
$string['add_question'] = 'Add a new question';
$string['add_option'] = 'Add a new option';
$string['activate'] = 'Make the vote active';
$string['delete_cache'] = 'Reset the vote cache';

$string['already_voted'] = 'Thank you for voting, please come back after {$a->close} to see the results.';

$string['eliminated'] = 'Eliminated in round {$a->round}';
$string['votes'] = '({$a->votes} votes)';

// Strings to describe the different types of vote.
$string['type_poll'] = 'Poll';
$string['type_poll_help'] = 'This is a simple poll: The user chooses a single option from each question and will then be shown the'.
        ' results immediately.';
$string['type_vote'] = 'Vote';
$string['type_vote_help'] = 'This is a simple poll: The user chooses a single option from each question, but the results are not'.
        ' visible until the voting has ended.';
$string['type_av'] = 'Alternative vote';
$string['type_av_help'] = 'In this system each user ranks at least one option from each question. If no option has more than 50% of'.
        ' the vote, then the option with the lowest number of votes will be discarded and the next options of the people who'.
        ' voted for it will be used instead. This will continue until one result has over 50% of the votes.';
$string['type_unknown'] = 'This vote has an invaild vote type.';

// Strings for the config form.
$string['closedate'] = 'Close date';
$string['closedate_help'] = 'Sets the last date that votes will be accepted.';
$string['type'] = 'Vote type';
$string['type_help'] = 'Sets how the vote will behave.'.html_writer::empty_tag('br').
        html_writer::tag('strong', $string['type_poll']).' - '.$string['type_poll_help'].html_writer::empty_tag('br').
        html_writer::tag('strong', $string['type_vote']).' - '.$string['type_vote_help'].html_writer::empty_tag('br').
        html_writer::tag('strong', $string['type_av']).' - '.$string['type_av_help'];
$string['displayinline'] = 'Display inline';
$string['displayinline_help'] = 'If set to yes the vote form will be displayed on the course page.';
$string['votestate'] = 'Vote active';
$string['votestate_help'] = 'To edit the questions the vote must not be active.

The activity will not be available to students until it is active.';

// Strings for the question and option forms.
$string['question_form'] = 'Question form';
$string['question'] = 'Question';
$string['question_required'] = 'You must enter a question';
$string['question_help'] = 'The question that will be displayed to users.';
$string['weighting'] = 'Weighting';
$string['weighting_help'] = 'Changes the order of the question (1 is highest up the list, 20 the lowest). If more than one '.
        'question has the same weight they will be ordered alphabetically.';
$string['option_form'] = 'Option form';
$string['option'] = 'Option';
$string['option_required'] = 'You must enter text for the option';
$string['option_help'] = 'An option that will be displayed to users.';

// Strings for the vote form.
$string['vote_form'] = 'Voting form';
$string['no_choice'] = 'You must make rank at least one option.';
$string['duplicate'] = 'You can only select a rank one time per question.';
$string['invalid_choice'] = 'You must rank the options consecutively from 1.';
$string['noquestions'] = 'No questions have been added to this vote.';

// Strings for the capabilities.
$string['vote:view'] = 'View vote forms';
$string['vote:submit'] = 'Can submit votes';
$string['vote:edit'] = 'Can edit vote forms';
$string['vote:addinstance'] = 'Can add the activity to courses (needed in moodle 2.4)';

// Completion strings.
$string['completionvoted'] = 'Vote to complete this activity.';
$string['completionvotedgroup'] = 'Require vote';

// Overview message.
$string['activityoverview'] = 'You have votes closing in the next seven days.';
$string['overviewname'] = 'Vote: {$a->link}';
$string['overviewmessage'] = 'Close date: {$a->closedate}';
