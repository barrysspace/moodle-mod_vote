@mod @mod_vote @mod_vote_altvote @uon
Feature: Test the creation and use of vote activity set as 'Alternative' vote type
    In order to allow students to participate in a vote activity (Alternative vote type)
    As a teacher
    I need to be able to add vote activities to a course.

  Background:
    Given the following "users" exist:
        | username | firstname | lastname | email            |
        | teacher1 | Teacher   | 1        | teacher1@asd.com |
        | student1 | Student   | 1        | student1@asd.com |
        | student2 | Student   | 2        | student2@asd.com |
        | student3 | Student   | 3        | student3@asd.com |
    And the following "courses" exist:
        | fullname | shortname | category |
        | Course 1 | C1        | 0        |
    And the following "course enrolments" exist:
        | user     | course | role           |
        | teacher1 | C1     | editingteacher |
        | student1 | C1     | student        |
        | student2 | C1     | student        |
        | student3 | C1     | student        |
    And I log in as "teacher1"
    And I follow "Course 1"
    And I turn editing mode on
    And I add a "Vote" to section "1" and I fill the form with:
        | Vote name | Vote test 1 |
        | Description | Vote description 1 |
        | id_votetype_3 | Alternative vote |
        | Visible | Show |
    And I follow "Vote test 1"
    And I follow "Add a new question"
    And I set the following fields to these values:
        | Question | What colour is the sky? |
    And I press "Save changes"
    And I follow "Add a new option"
    And I set the following fields to these values:
        | Option | Green |
    And I press "Save changes"
    And I follow "Add a new option"
    And I set the following fields to these values:
        | Option | Blue |
    And I press "Save changes"
    And I follow "Add a new option"
    And I set the following fields to these values:
        | Option | Red |
    And I press "Save changes"
    And I follow "Add a new option"
    And I set the following fields to these values:
        | Option | Yellow |
    And I press "Save changes"
    And I follow "Make the vote active"
    And I log out

  @javascript
  Scenario: View vote activity select an option as a student
    Given I log in as "student1"
    And I follow "Course 1"
    When I follow "Vote test 1"
    Then I should see "Green"
    And I should see "Blue"
    And I should see "Yellow"
    And I should see "Red"
    And I set the following fields to these values:
     | Blue | 4 |
     | Green | 3 |
     | Red | 2 |
     | Yellow | 1 |
    Given I press "Save changes"
    Then I should see "Thank you for voting"
    And I log out

    Given I log in as "student2"
    And I follow "Course 1"
    When I follow "Vote test 1"
    Then I should see "Green"
    And I should see "Blue"
    And I should see "Yellow"
    And I should see "Red"
    And I set the following fields to these values:
     | Blue | 1 |
     | Green | 2 |
     | Red | 3 |
     | Yellow | 4 |
    Given I press "Save changes"
    Then I should see "Thank you for voting"
    And I log out

    Given I log in as "student3"
    And I follow "Course 1"
    When I follow "Vote test 1"
    Then I should see "Green"
    And I should see "Blue"
    And I should see "Yellow"
    And I should see "Red"
    And I set the following fields to these values:
     | Blue | 3 |
     | Green | 4 |
     | Red | 1 |
     | Yellow | 2 |
    Given I press "Save changes"
    Then I should see "Thank you for voting"
    And I log out

    Given I log in as "teacher1"
    And I follow "Course 1"
    And I turn editing mode on
    When I follow "Vote test 1"
    And I navigate to "Edit settings" node in "Vote administration"
    And I set the following fields to these values:
     | closedate[year] | 2014 |
    And I press "Save and display"
    And I follow "Vote test 1"
    Then I should see "Red (2 votes)"
    And I should see "Yellow (1 votes)"
    And I should see "Blue (1 votes) Eliminated in round 2"
    And I should see "Green (0 votes) Eliminated in round 1"
