<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Tests the vote activities mod_vote_votelib.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 * @group uon
 */
class mod_vote_votelib_testcase extends advanced_testcase {
    /**
     * Tests that mod_vote_votelib is able to process the results from a mode_vote_form correctly.
     *
     * @covers mod_vote_votelib::process_vote
     * @group mod_vote
     * @group uon
     */
    public function test_process_vote() {
        global $DB;
        $this->resetAfterTest(true);

        require_once(dirname(__DIR__).'/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        // Create two vote activities.
        $user0 = self::getDataGenerator()->create_user();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();

        $course0 = self::getDataGenerator()->create_course();

        // Enrol some users onto the course.
        self::getDataGenerator()->enrol_user($user0->id, $course0->id);
        self::getDataGenerator()->enrol_user($user1->id, $course0->id);
        self::getDataGenerator()->enrol_user($user2->id, $course0->id);

        // Add a vote activity to the course.
        $vote0 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_POLL));
        $question0 = $votegenerator->create_question(
                $vote0,
                array('question' => 'Test question'),
                array(
                    array('optionname' => 'First option'),
                    array('optionname' => 'Second option'),
                    array('optionname' => 'Third option'),
                    array('optionname' => 'Forth option'),
                ));

        $vote1 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_AV));
        $question1 = $votegenerator->create_question(
                $vote1,
                array('question' => 'Test question 2'),
                array(
                    array('optionname' => 'Option 1'),
                    array('optionname' => 'Option 4'),
                    array('optionname' => 'Option 3'),
                    array('optionname' => 'Option 2'),
                ));
        $question2 = $votegenerator->create_question(
                $vote1,
                array('question' => 'Test question 3'),
                array(
                    array('optionname' => 'Option 1'),
                    array('optionname' => 'Option 4'),
                    array('optionname' => 'Option 3'),
                    array('optionname' => 'Option 2'),
                ));

        $this->assertEquals(0, $DB->count_records('vote_votes'));

        // Setup complete, create the results of a voting form for a poll..
        $this->setUser($user0);
        $voterecord = new stdClass();
        $voterecord->voteid = $vote0->id;
        $voterecord->vt = $vote0->votetype;
        // Polls and votes will pass votes via parameters in the form: question-<questionid>
        $voterecord->{'question-'.$question0->id} = $question0->options[0]->id;
        mod_vote_votelib::process_vote($voterecord);

        $votes = $DB->get_records('vote_votes',
                array('voteid' => $vote0->id, 'optionid' => $question0->options[0]->id, 'userid' => $user0->id));
        $this->assertCount(1, $votes);
        $this->assertAttributeEquals(1, 'vote', array_shift($votes));
        $this->assertEquals(1, $DB->count_records('vote_votes')); // Only a single record should exist.

        // Check that voting with a second user does not impact the first result.
        $this->setUser($user1);
        $voterecord2 = new stdClass();
        $voterecord2->voteid = $vote0->id;
        $voterecord2->vt = $vote0->votetype;
        $voterecord2->{'question-'.$question0->id} = $question0->options[2]->id;
        mod_vote_votelib::process_vote($voterecord2);

        $votes2 = $DB->get_records('vote_votes',
                array('voteid' => $vote0->id, 'optionid' => $question0->options[2]->id, 'userid' => $user1->id));
        $this->assertCount(1, $votes2);
        $this->assertAttributeEquals(1, 'vote', array_shift($votes2));
        $this->assertEquals(2, $DB->count_records('vote_votes'));

        // Test AV voting.
        $this->setUser($user0);
        $voterecord3 = new stdClass();
        $voterecord3->voteid = $vote1->id;
        $voterecord3->vt = $vote1->votetype;
        // AV will pass ranks vai a parameter named in the form: option-<questionid>-<optionid>.
        $voterecord3->{'option-'.$question1->id.'-'.$question1->options[0]->id} = 1; // All of question 1 ranked.
        $voterecord3->{'option-'.$question1->id.'-'.$question1->options[1]->id} = 3;
        $voterecord3->{'option-'.$question1->id.'-'.$question1->options[2]->id} = 4;
        $voterecord3->{'option-'.$question1->id.'-'.$question1->options[3]->id} = 2;
        $voterecord3->{'option-'.$question2->id.'-'.$question2->options[0]->id} = 2; // Two options in question 2 ranked.
        $voterecord3->{'option-'.$question2->id.'-'.$question2->options[1]->id} = 0;
        $voterecord3->{'option-'.$question2->id.'-'.$question2->options[2]->id} = 0;
        $voterecord3->{'option-'.$question2->id.'-'.$question2->options[3]->id} = 1;
        mod_vote_votelib::process_vote($voterecord3);

        $votes3 = $DB->get_records('vote_votes', array('voteid' => $vote1->id, 'userid' => $user0->id), 'optionid');
        $this->assertInternalType('array', $votes3);
        $this->assertCount(6, $votes3);
        $thisvote = array_shift($votes3);
        $this->assertAttributeEquals($question1->options[0]->id, 'optionid', $thisvote);
        $this->assertAttributeEquals(1, 'vote', $thisvote);
        $thisvote = array_shift($votes3);
        $this->assertAttributeEquals($question1->options[1]->id, 'optionid', $thisvote);
        $this->assertAttributeEquals(3, 'vote', $thisvote);
        $thisvote = array_shift($votes3);
        $this->assertAttributeEquals($question1->options[2]->id, 'optionid', $thisvote);
        $this->assertAttributeEquals(4, 'vote', $thisvote);
        $thisvote = array_shift($votes3);
        $this->assertAttributeEquals($question1->options[3]->id, 'optionid', $thisvote);
        $this->assertAttributeEquals(2, 'vote', $thisvote);
        $thisvote = array_shift($votes3);
        $this->assertAttributeEquals($question2->options[0]->id, 'optionid', $thisvote);
        $this->assertAttributeEquals(2, 'vote', $thisvote);
        $thisvote = array_shift($votes3);
        $this->assertAttributeEquals($question2->options[3]->id, 'optionid', $thisvote);
        $this->assertAttributeEquals(1, 'vote', $thisvote);

        $this->assertEquals(8, $DB->count_records('vote_votes'));

        $this->assertDebuggingNotCalled();
    }
}
