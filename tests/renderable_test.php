<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Tests the vote activities mod_vote_renderable.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 */
class mod_vote_renderable_testcase extends advanced_testcase {
    /**
     * Tests that mod_vote_renderable works correctly.
     *
     * @covers mod_vote_renderable::get_questions
     * @covers mod_vote_renderable::get_results
     * @covers mod_vote_renderable::has_voted
     * @covers mod_vote_renderable::can_submit
     * @covers mod_vote_renderable::can_edit
     * @covers mod_vote_renderable::results_visible
     * @group mod_vote
     * @group uon
     */
    public function test_renderable() {
        global $DB;
        $this->resetAfterTest(true);

        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        // Setup some vote activities.
        $course0 = self::getDataGenerator()->create_course();

        $teacherid = $DB->get_field('role', 'id', array('shortname' => 'editingteacher'), MUST_EXIST);
        $studentid = $DB->get_field('role', 'id', array('shortname' => 'student'), MUST_EXIST);

        $user0 = self::getDataGenerator()->create_user();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $user4 = self::getDataGenerator()->create_user(); // This user will not vote.

        self::getDataGenerator()->enrol_user($user0->id, $course0->id, $teacherid); // We want one teacher.
        self::getDataGenerator()->enrol_user($user1->id, $course0->id, $studentid);
        self::getDataGenerator()->enrol_user($user2->id, $course0->id, $studentid);
        self::getDataGenerator()->enrol_user($user3->id, $course0->id, $studentid);
        self::getDataGenerator()->enrol_user($user4->id, $course0->id, $studentid);

        // Create a course and add a vote activity to it that closes in the future.
        $vote0 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_POLL));
        $question0 = $votegenerator->create_question(
                $vote0,
                array('question' => 'Test question'),
                array(
                    array('optionname' => 'First option'), // Will order alphabetically.
                    array('optionname' => 'Second option'),
                    array('optionname' => 'Third option'),
                    array('optionname' => 'Forth option'),
                ));
        $votegenerator->create_votes($user0, $question0, array($question0->options[2]));
        $votegenerator->create_votes($user1, $question0, array($question0->options[0]));
        $votegenerator->create_votes($user2, $question0, array($question0->options[0]));
        $votegenerator->create_votes($user3, $question0, array($question0->options[1]));

        // An AV, that has passed it's close date.
        $vote1 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_AV, 'closedate' => (time() - 1000)));
        $question1 = $votegenerator->create_question(
                $vote1,
                array('question' => 'Test question 2'),
                array(
                    array('optionname' => 'Option 1', 'sortorder' => 2), // Should now be last.
                    array('optionname' => 'Option 4'),
                    array('optionname' => 'Option 3'),
                    array('optionname' => 'Option 2'),
                ));
        $votegenerator->create_votes($user0, $question1, array($question1->options[0], $question1->options[2]));
        $votegenerator->create_votes($user1, $question1, array($question1->options[1]));
        $votegenerator->create_votes($user2, $question1, array($question1->options[2]));
        $votegenerator->create_votes($user3, $question1, array($question1->options[3], $question1->options[2]));
        // Setup completed.

        // First test with the teacher.
        $this->setUser($user0);
        $renderable0 = new mod_vote_renderable($vote0->id);
        $this->assertFalse($renderable0->closed);
        $this->assertTrue($renderable0->can_edit());
        $this->assertFalse($renderable0->can_submit()); // They should not be able to vote, but we have cheated.
        $this->assertTrue($renderable0->has_voted());
        $this->assertTrue($renderable0->results_visible()); // It is a poll and they have a vote.

        $questions = $renderable0->get_questions();
        $this->assertCount(1, $questions);
        $this->assertAttributeEquals($question0->id, 'id', $questions[0]);
        $this->assertAttributeEquals($question0->question, 'question', $questions[0]);
        $this->assertCount(4, $questions[0]->options);
        $this->assertAttributeEquals($question0->options[0]->id, 'id', $questions[0]->options[0]);
        $this->assertAttributeEquals($question0->options[0]->optionname, 'name', $questions[0]->options[0]);
        $this->assertAttributeEquals($question0->options[3]->id, 'id', $questions[0]->options[1]);
        $this->assertAttributeEquals($question0->options[3]->optionname, 'name', $questions[0]->options[1]);
        $this->assertAttributeEquals($question0->options[1]->id, 'id', $questions[0]->options[2]);
        $this->assertAttributeEquals($question0->options[1]->optionname, 'name', $questions[0]->options[2]);
        $this->assertAttributeEquals($question0->options[2]->id, 'id', $questions[0]->options[3]);
        $this->assertAttributeEquals($question0->options[2]->optionname, 'name', $questions[0]->options[3]);

        $results0 = $renderable0->get_results();
        $this->assertCount(1, $results0);
        $this->assertAttributeEquals($question0->id, 'id', $results0[0]);
        $this->assertAttributeEquals($question0->question, 'question', $results0[0]);
        $this->assertAttributeEquals(0, 'rounds', $results0[0]);
        $this->assertAttributeEquals(2, 'maxresult', $results0[0]);
        $this->assertCount(4, $results0[0]->options);
        $this->assertAttributeEquals($question0->options[0]->id, 'id', $results0[0]->options[0]);
        $this->assertAttributeEquals($question0->options[0]->optionname, 'name', $results0[0]->options[0]);
        $this->assertAttributeEquals(2, 'result', $results0[0]->options[0]);
        $this->assertAttributeEquals(0, 'round', $results0[0]->options[0]);
        $this->assertAttributeEquals($question0->options[1]->id, 'id', $results0[0]->options[1]);
        $this->assertAttributeEquals($question0->options[1]->optionname, 'name', $results0[0]->options[1]);
        $this->assertAttributeEquals(1, 'result', $results0[0]->options[1]);
        $this->assertAttributeEquals(0, 'round', $results0[0]->options[1]);
        $this->assertAttributeEquals($question0->options[2]->id, 'id', $results0[0]->options[2]);
        $this->assertAttributeEquals($question0->options[2]->optionname, 'name', $results0[0]->options[2]);
        $this->assertAttributeEquals(1, 'result', $results0[0]->options[2]);
        $this->assertAttributeEquals(0, 'round', $results0[0]->options[2]);
        $this->assertAttributeEquals($question0->options[3]->id, 'id', $results0[0]->options[3]);
        $this->assertAttributeEquals($question0->options[3]->optionname, 'name', $results0[0]->options[3]);
        $this->assertAttributeEquals(0, 'result', $results0[0]->options[3]);
        $this->assertAttributeEquals(0, 'round', $results0[0]->options[3]);

        $renderable1 = new mod_vote_renderable($vote1->id);
        $this->assertTrue($renderable1->closed);
        $this->assertTrue($renderable1->can_edit());
        $this->assertFalse($renderable1->can_submit());
        $this->assertTrue($renderable1->has_voted());
        $this->assertTrue($renderable1->results_visible()); // Close date passed.
        
        $questions1 = $renderable1->get_questions();
        $this->assertCount(1, $questions1);
        $this->assertAttributeEquals($question1->id, 'id', $questions1[0]);
        $this->assertAttributeEquals($question1->question, 'question', $questions1[0]);
        $this->assertCount(4, $questions1[0]->options);
        $this->assertAttributeEquals($question1->options[3]->id, 'id', $questions1[0]->options[0]);
        $this->assertAttributeEquals($question1->options[3]->optionname, 'name', $questions1[0]->options[0]);
        $this->assertAttributeEquals($question1->options[2]->id, 'id', $questions1[0]->options[1]);
        $this->assertAttributeEquals($question1->options[2]->optionname, 'name', $questions1[0]->options[1]);
        $this->assertAttributeEquals($question1->options[1]->id, 'id', $questions1[0]->options[2]);
        $this->assertAttributeEquals($question1->options[1]->optionname, 'name', $questions1[0]->options[2]);
        $this->assertAttributeEquals($question1->options[0]->id, 'id', $questions1[0]->options[3]);
        $this->assertAttributeEquals($question1->options[0]->optionname, 'name', $questions1[0]->options[3]);

        $results1 = $renderable1->get_results();
        $this->assertCount(1, $results1);
        $this->assertAttributeEquals($question1->id, 'id', $results1[0]);
        $this->assertAttributeEquals($question1->question, 'question', $results1[0]);
        $this->assertAttributeEquals(2, 'rounds', $results1[0]);
        $this->assertAttributeEquals(2, 'maxresult', $results1[0]);
        $this->assertCount(4, $results1[0]->options);
        $this->assertAttributeEquals($question1->options[2]->id, 'id', $results1[0]->options[0]);
        $this->assertAttributeEquals($question1->options[2]->optionname, 'name', $results1[0]->options[0]);
        $this->assertAttributeEquals(2, 'result', $results1[0]->options[0]);
        $this->assertAttributeEquals(2, 'round', $results1[0]->options[0]);
        $this->assertAttributeEquals($question1->options[3]->id, 'id', $results1[0]->options[1]);
        $this->assertAttributeEquals($question1->options[3]->optionname, 'name', $results1[0]->options[1]);
        $this->assertAttributeEquals(1, 'result', $results1[0]->options[1]);
        $this->assertAttributeEquals(2, 'round', $results1[0]->options[1]);
        $this->assertAttributeEquals($question1->options[1]->id, 'id', $results1[0]->options[2]);
        $this->assertAttributeEquals($question1->options[1]->optionname, 'name', $results1[0]->options[2]);
        $this->assertAttributeEquals(1, 'result', $results1[0]->options[2]);
        $this->assertAttributeEquals(2, 'round', $results1[0]->options[2]);
        $this->assertAttributeEquals($question1->options[0]->id, 'id', $results1[0]->options[3]);
        $this->assertAttributeEquals($question1->options[0]->optionname, 'name', $results1[0]->options[3]);
        $this->assertAttributeEquals(1, 'result', $results1[0]->options[3]);
        $this->assertAttributeEquals(1, 'round', $results1[0]->options[3]);

        // Test a student $user1 who has voted.
        $this->setUser($user1);
        $renderable2 = new mod_vote_renderable($vote0->id);
        $this->assertFalse($renderable2->closed);
        $this->assertFalse($renderable2->can_edit());
        $this->assertFalse($renderable2->can_submit()); // A user may only vote one time.
        $this->assertTrue($renderable2->has_voted());
        $this->assertTrue($renderable2->results_visible()); // It is a poll and they have a vote.

        $renderable3 = new mod_vote_renderable($vote1->id);
        $this->assertTrue($renderable3->closed);
        $this->assertFalse($renderable3->can_edit());
        $this->assertFalse($renderable3->can_submit()); // Voting closed.
        $this->assertTrue($renderable3->has_voted());
        $this->assertTrue($renderable3->results_visible()); // Close date passed.

        // Test a student $user4 who has not voted.
        $this->setUser($user4);
        $renderable4 = new mod_vote_renderable($vote0->id);
        $this->assertFalse($renderable4->closed);
        $this->assertFalse($renderable4->can_edit());
        $this->assertTrue($renderable4->can_submit());
        $this->assertFalse($renderable4->has_voted());
        $this->assertFalse($renderable4->results_visible()); // They have not voted.

        $renderable5 = new mod_vote_renderable($vote1->id);
        $this->assertTrue($renderable5->closed);
        $this->assertFalse($renderable5->can_edit());
        $this->assertFalse($renderable5->can_submit()); // Voting closed.
        $this->assertFalse($renderable5->has_voted());
        $this->assertTrue($renderable5->results_visible()); // Close date passed.
    }
}
