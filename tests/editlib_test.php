<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Tests the vote activities mod_vote_cachelib.
 *
 * @package     mod_vote
 * @copyright   University of Nottingham, 2014
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group mod_vote
 * @group uon
 */
class mod_vote_editlib_testcase extends advanced_testcase {
    /**
     * Tests that mod_vote_editlib is able to add a question correctly.
     *
     * @covers mod_vote_editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion() {
        global $DB;
        $this->resetAfterTest(true);

        // Test no questions exist.
        $this->assertEquals(0, $DB->count_records('vote_question'));

        // Add a question.
        $question = new stdClass();
        $question->voteid = 10;
        $question->question = 'Test Question';
        $question->sortorder = 1;
        mod_vote_editlib::process_submittedquestion($question);

        // Test it was created correctly.
        $this->assertEquals(1, $DB->count_records('vote_question'));
        $questionrecord = $DB->get_record('vote_question', array('voteid' => $question->voteid));
        $this->assertAttributeEquals($question->voteid, 'voteid', $questionrecord);
        $this->assertAttributeEquals($question->question, 'question', $questionrecord);
        $this->assertAttributeEquals($question->sortorder, 'sortorder', $questionrecord);

        $question2 = new stdClass();
        $question2->voteid = 11;
        $question2->question = 'Some question';
        $question2->sortorder = 5;
        mod_vote_editlib::process_submittedquestion($question2);

        $this->assertEquals(2, $DB->count_records('vote_question'));
        $questionrecord2 = $DB->get_record('vote_question', array('voteid' => $question2->voteid));
        $this->assertAttributeEquals($question2->voteid, 'voteid', $questionrecord2);
        $this->assertAttributeEquals($question2->question, 'question', $questionrecord2);
        $this->assertAttributeEquals($question2->sortorder, 'sortorder', $questionrecord2);

        // Test that the function updates correctly.
        $question3 = new stdClass();
        $question3->q = $questionrecord->id; // q should be passed by the form if it edited a question.
        $question3->voteid = 10;
        $question3->question = 'The question text has been changed';
        $question3->sortorder = 2;
        mod_vote_editlib::process_submittedquestion($question3);

        $this->assertEquals(2, $DB->count_records('vote_question'));
        $questionrecord3 = $DB->get_record('vote_question', array('voteid' => $question3->voteid));
        $this->assertAttributeEquals($question3->voteid, 'voteid', $questionrecord3);
        $this->assertAttributeEquals($question3->question, 'question', $questionrecord3);
        $this->assertAttributeEquals($question3->sortorder, 'sortorder', $questionrecord3);

        // Check that the other question was not modified.
        $questionrecord4 = $DB->get_record('vote_question', array('voteid' => $question2->voteid));
        $this->assertEquals($questionrecord2, $questionrecord4);

        // Test adding a really long question string.
        // Test that the function updates correctly.
        $question4 = new stdClass();
        $question4->q = $questionrecord->id; // q should be passed by the form if it edited a question.
        $question4->voteid = 10;
        // 300 characters is larger than the database field.
        $question4->question = '1234566789012345667890123456678901234566789012345667890123456678901234566789012345667890123'
                . '456678901234566789012345667890123456678901234566789012345667890123456678901234566789012345667890123456678'
                . '901234566789012345667890123456678901234566789012345667890123456678901234566789012345667890123456678901234'
                . '56678901234566789012345667890123456678901234566789012345667890';
        $question4->sortorder = 2;
        mod_vote_editlib::process_submittedquestion($question4);

        $this->assertEquals(2, $DB->count_records('vote_question'));
        $questionrecord5 = $DB->get_record('vote_question', array('voteid' => $question4->voteid));
        $this->assertAttributeEquals($question4->voteid, 'voteid', $questionrecord5);
        $this->assertAttributeEquals(substr($question4->question, 0, 255), 'question', $questionrecord5);
        $this->assertAttributeEquals($question4->sortorder, 'sortorder', $questionrecord5);

        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that mod_vote_editlib fails correctly when a voteid is not passed.
     *
     * @expectedException coding_exception
     * @expectedExceptionMessage $question->voteid was not set in mod_vote_editlib::process_submittedquestion
     *
     * @covers mod_vote_editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion_no_voteid() {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $question = new stdClass();
        $question->question = 'Test Question';
        $question->sortorder = 1;
        mod_vote_editlib::process_submittedquestion($question);
    }

    /**
     * Tests that mod_vote_editlib fails correctly when a question is not passed.
     *
     * @expectedException coding_exception
     * @expectedExceptionMessage $question->question was not set in mod_vote_editlib::process_submittedquestion
     *
     * @covers mod_vote_editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion_no_question() {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $question = new stdClass();
        $question->voteid = 10;
        $question->sortorder = 1;
        mod_vote_editlib::process_submittedquestion($question);
    }

    /**
     * Tests that mod_vote_editlib fails correctly when a sortorder is not passed.
     *
     * @expectedException coding_exception
     * @expectedExceptionMessage $question->sortorder was not set in mod_vote_editlib::process_submittedquestion
     *
     * @covers mod_vote_editlib::process_submittedquestion
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedquestion_no_sortorder() {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $question = new stdClass();
        $question->voteid = 10;
        $question->question = 'Test Question';
        mod_vote_editlib::process_submittedquestion($question);
    }

    /**
     * Tests that mod_vote_editlib is able to add a options correctly.
     *
     * @covers mod_vote_editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption() {
        global $DB;
        $this->resetAfterTest(true);

        // Test no options exist.
        $this->assertEquals(0, $DB->count_records('vote_options'));

        // Add a question.
        $option = new stdClass();
        $option->voteid = 10;
        $option->q = 13; // The question's id.
        $option->optionname = 'Test Option';
        $option->sortorder = 1;
        mod_vote_editlib::process_submittedoption($option);

        // Test it was created correctly.
        $this->assertEquals(1, $DB->count_records('vote_options'));
        $optionrecord = $DB->get_record('vote_options', array('questionid' => $option->q));
        $this->assertAttributeEquals($option->voteid, 'voteid', $optionrecord);
        $this->assertAttributeEquals($option->q, 'questionid', $optionrecord);
        $this->assertAttributeEquals($option->optionname, 'optionname', $optionrecord);
        $this->assertAttributeEquals($option->sortorder, 'sortorder', $optionrecord);

        // Create a second option on the same vote.
        $option2 = new stdClass();
        $option2->voteid = 10;
        $option2->q = 14;
        $option2->optionname = 'Another test Option';
        $option2->sortorder = 2;
        mod_vote_editlib::process_submittedoption($option2);

        // Test it was created correctly.
        $this->assertEquals(2, $DB->count_records('vote_options'));
        $optionrecord2 = $DB->get_record('vote_options', array('questionid' => $option2->q));
        $this->assertAttributeEquals($option2->voteid, 'voteid', $optionrecord2);
        $this->assertAttributeEquals($option2->q, 'questionid', $optionrecord2);
        $this->assertAttributeEquals($option2->optionname, 'optionname', $optionrecord2);
        $this->assertAttributeEquals($option2->sortorder, 'sortorder', $optionrecord2);

        // Test updating an option.
        $option3 = new stdClass();
        $option3->o = $optionrecord->id;
        $option3->voteid = 10;
        $option3->q = 13;
        $option3->optionname = 'Changed Test Option';
        $option3->sortorder = 6;
        mod_vote_editlib::process_submittedoption($option3);

        // Test it was created correctly.
        $this->assertEquals(2, $DB->count_records('vote_options'));
        $optionrecord3 = $DB->get_record('vote_options', array('questionid' => $option->q));
        $this->assertAttributeEquals($option3->voteid, 'voteid', $optionrecord3);
        $this->assertAttributeEquals($option3->q, 'questionid', $optionrecord3);
        $this->assertAttributeEquals($option3->optionname, 'optionname', $optionrecord3);
        $this->assertAttributeEquals($option3->sortorder, 'sortorder', $optionrecord3);

        // Check that the other option has not changed.
        $optionrecord4 = $DB->get_record('vote_options', array('questionid' => $option2->q));
        $this->assertEquals($optionrecord2, $optionrecord4);

        // Test a long option name.
        $option4 = new stdClass();
        $option4->o = $optionrecord->id;
        $option4->voteid = 10;
        $option4->q = 13;
        $option4->optionname = 'Changed Test Option';
        $option4->sortorder = 6;
        mod_vote_editlib::process_submittedoption($option4);

        // Test it was created correctly.
        $this->assertEquals(2, $DB->count_records('vote_options'));
        $optionrecord5 = $DB->get_record('vote_options', array('questionid' => $option->q));
        $this->assertAttributeEquals($option4->voteid, 'voteid', $optionrecord5);
        $this->assertAttributeEquals($option4->q, 'questionid', $optionrecord5);
        $this->assertAttributeEquals(substr($option4->optionname, 0, 255), 'optionname', $optionrecord5);
        $this->assertAttributeEquals($option4->sortorder, 'sortorder', $optionrecord5);

        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that mod_vote_editlib::process_submittedoption fails correctly when a voteid is not passed.
     *
     * @expectedException coding_exception
     * @expectedExceptionMessage $option->voteid was not set in mod_vote_editlib::process_submittedoption
     *
     * @covers mod_vote_editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_voteid() {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->q = 13; // The question's id.
        $option->optionname = 'Test Option';
        $option->sortorder = 1;
        mod_vote_editlib::process_submittedoption($option);
    }

    /**
     * Tests that mod_vote_editlib::process_submittedoption fails correctly when the question id is not passed.
     *
     * @expectedException coding_exception
     * @expectedExceptionMessage The question id ($option->q) was not set in mod_vote_editlib::process_submittedoption
     *
     * @covers mod_vote_editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_questionid() {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->voteid = 10;
        $option->optionname = 'Test Option';
        $option->sortorder = 1;
        mod_vote_editlib::process_submittedoption($option);
    }

    /**
     * Tests that mod_vote_editlib::process_submittedoption fails correctly when a optionname is not passed.
     *
     * @expectedException coding_exception
     * @expectedExceptionMessage $option->optionname was not set in mod_vote_editlib::process_submittedoption
     *
     * @covers mod_vote_editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_optionname() {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->voteid = 10;
        $option->q = 13; // The question's id.
        $option->sortorder = 1;
        mod_vote_editlib::process_submittedoption($option);
    }

    /**
     * Tests that mod_vote_editlib::process_submittedoption fails correctly when a sortorder is not passed.
     *
     * @expectedException coding_exception
     * @expectedExceptionMessage $option->sortorder was not set in mod_vote_editlib::process_submittedoption
     *
     * @covers mod_vote_editlib::process_submittedoption
     * @group mod_vote
     * @group uon
     */
    public function test_process_submittedoption_no_sortorder() {
        $this->resetAfterTest(true);

        // Test that it fails if required information is not passed.
        $option = new stdClass();
        $option->voteid = 10;
        $option->q = 13; // The question's id.
        $option->optionname = 'Test Option';
        mod_vote_editlib::process_submittedoption($option);
    }

    /**
     * Tests that mod_vote_editlib deletes correctly.
     *
     * @covers mod_vote_editlib::delete_option
     * @covers mod_vote_editlib::delete_question
     * @group mod_vote
     * @group uon
     */
    public function test_delete() {
        global $DB;
        $this->resetAfterTest(true);

        require_once(dirname(__DIR__).'/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        // Create two vote activities and generate their cache.
        $user0 = self::getDataGenerator()->create_user();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $user3 = self::getDataGenerator()->create_user();
        $user4 = self::getDataGenerator()->create_user();

        // Create a course and add a vote activity to it.
        $course0 = self::getDataGenerator()->create_course();
        $vote0 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_POLL));
        $question0 = $votegenerator->create_question(
                $vote0,
                array('question' => 'Test question'),
                array(
                    array('optionname' => 'First option'),
                    array('optionname' => 'Second option'),
                    array('optionname' => 'Third option'),
                    array('optionname' => 'Forth option'),
                ));

        // Enrol some users onto the course and get them to vote in the poll.
        self::getDataGenerator()->enrol_user($user0->id, $course0->id);
        self::getDataGenerator()->enrol_user($user1->id, $course0->id);
        self::getDataGenerator()->enrol_user($user2->id, $course0->id);
        self::getDataGenerator()->enrol_user($user3->id, $course0->id);
        self::getDataGenerator()->enrol_user($user4->id, $course0->id);

        $votegenerator->create_votes($user0, $question0, array($question0->options[2]));
        $votegenerator->create_votes($user1, $question0, array($question0->options[0]));
        $votegenerator->create_votes($user2, $question0, array($question0->options[0]));
        $votegenerator->create_votes($user3, $question0, array($question0->options[1]));
        $votegenerator->create_votes($user4, $question0, array($question0->options[3]));

        // Get the cached results.
        $results = mod_vote_cachelib::get_cached_results(new mod_vote_renderable($vote0->id));

        $vote1 = $votegenerator->create_instance(array('course' => $course0->id, 'votetype' => VOTE_TYPE_AV));
        $question1 = $votegenerator->create_question(
                $vote1,
                array('question' => 'Test question 2'),
                array(
                    array('optionname' => 'Option 1'),
                    array('optionname' => 'Option 4'),
                    array('optionname' => 'Option 3'),
                    array('optionname' => 'Option 2'),
                ));
        $question2 = $votegenerator->create_question(
                $vote1,
                array('question' => 'Test question 3'),
                array(
                    array('optionname' => 'Option 1'),
                    array('optionname' => 'Option 4'),
                    array('optionname' => 'Option 3'),
                    array('optionname' => 'Option 2'),
                ));

        $votegenerator->create_votes($user0, $question1, array($question1->options[0], $question1->options[2]));
        $votegenerator->create_votes($user1, $question1, array($question1->options[1]));
        $votegenerator->create_votes($user2, $question1, array($question1->options[2]));
        $votegenerator->create_votes($user3, $question1, array($question1->options[3], $question1->options[2]));
        $votegenerator->create_votes($user4, $question1, array($question1->options[1]));

        $votegenerator->create_votes($user1, $question2, array($question2->options[0], $question2->options[2]));
        $votegenerator->create_votes($user0, $question2, array($question2->options[1], $question2->options[2], $question2->options[0]));
        $votegenerator->create_votes($user2, $question2, array($question2->options[2], $question2->options[3]));
        $votegenerator->create_votes($user4, $question2, array($question2->options[3], $question2->options[2]));
        $votegenerator->create_votes($user3, $question2, array($question2->options[1], $question2->options[2]));

        // Get the cached results.
        $results2 = mod_vote_cachelib::get_cached_results(new mod_vote_renderable($vote1->id));

        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(3, $DB->count_records('vote_question'));
        $this->assertEquals(12, $DB->count_records('vote_options'));
        $this->assertEquals(23, $DB->count_records('vote_votes'));
        $this->assertEquals(22, $DB->count_records('vote_result_cache'));

        // Setup completed, so start the tests.

        mod_vote_editlib::delete_option($question2->options[0]->id);
        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(3, $DB->count_records('vote_question'));
        $this->assertEquals(11, $DB->count_records('vote_options'));
        $this->assertEquals(21, $DB->count_records('vote_votes'));
        $this->assertEquals(21, $DB->count_records('vote_result_cache'));
        $this->assertEquals(0, $DB->count_records('vote_options', array('id' => $question2->options[0]->id)));
        $this->assertEquals(0, $DB->count_records('vote_votes', array('optionid' => $question2->options[0]->id)));
        $this->assertEquals(0, $DB->count_records('vote_result_cache', array('optionid' => $question2->options[0]->id)));

        mod_vote_editlib::delete_question($question1->id);
        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(2, $DB->count_records('vote_question'));
        $this->assertEquals(7, $DB->count_records('vote_options'));
        $this->assertEquals(14, $DB->count_records('vote_votes'));
        $this->assertEquals(12, $DB->count_records('vote_result_cache'));
        $this->assertEquals(0, $DB->count_records('vote_question', array('id' => $question1->id)));
        $this->assertEquals(0, $DB->count_records('vote_options', array('questionid' => $question1->id)));

        $this->assertDebuggingNotCalled();
    }

    /**
     * Tests that mod_vote_editlib can make a vote active.
     *
     * @covers mod_vote_editlib::make_active
     * @group mod_vote
     * @group uon
     */
    public function test_make_active() {
        global $DB;
        $this->resetAfterTest(true);

        require_once(dirname(__DIR__).'/lib.php');
        $votegenerator = self::getDataGenerator()->get_plugin_generator('mod_vote');

        // Test setup.
        $course0 = self::getDataGenerator()->create_course();
        $vote0 = $votegenerator->create_instance(array('course' => $course0->id, 'votestate' => VOTE_STATE_EDITING));
        $vote1 = $votegenerator->create_instance(array('course' => $course0->id, 'votestate' => VOTE_STATE_EDITING));

        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(0, $DB->count_records('vote', array('votestate' => VOTE_STATE_ACTIVE)));

        // Starts the test.
        mod_vote_editlib::make_active($vote0->id);
        $this->assertEquals(2, $DB->count_records('vote'));
        $this->assertEquals(1, $DB->count_records('vote', array('votestate' => VOTE_STATE_ACTIVE)));
        $this->assertEquals(1, $DB->count_records('vote', array('votestate' => VOTE_STATE_ACTIVE, 'id' => $vote0->id)));
    }
}
