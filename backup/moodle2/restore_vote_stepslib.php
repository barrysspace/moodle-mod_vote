<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod
 * @subpackage vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_vote_activity_task
 */

/**
 * Structure step to restore one vote activity
 */
class restore_vote_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('vote', '/activity/vote');
        $paths[] = new restore_path_element('vote_question', '/activity/vote/questions/question');
        $paths[] = new restore_path_element('vote_options', '/activity/vote/options/option');
        if ($userinfo) {

            $paths[] = new restore_path_element('vote_vote', '/activity/vote/vote_votes/vote_vote');
        }

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    protected function process_vote($data) {
        global $DB;

        $userinfo = $this->get_setting_value('userinfo');

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        if (!$userinfo) { // Only update these if user information is not being imported.
            $data->startdate = $this->apply_date_offset($data->startdate);
            $data->deadline = $this->apply_date_offset($data->deadline);
            $data->processed = 0;
            $data->state = VOTE_STATE_EDITING;
        }

        // Insert the vote record.
        $newitemid = $DB->insert_record('vote', $data);
        $this->apply_activity_instance($newitemid);
    }

    protected function process_vote_question($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->voteid = $this->get_new_parentid('vote');

        $newitemid = $DB->insert_record('vote_question', $data);
        $this->set_mapping('question', $oldid, $newitemid);
    }

    protected function process_vote_options($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->voteid = $this->get_new_parentid('vote');
        $data->questionid = $this->get_mappingid('question', $data->questionid);

        // Insert the entry record.
        $newitemid = $DB->insert_record('vote_options', $data);
        $this->set_mapping('option', $oldid, $newitemid);
    }

    protected function process_vote_vote($data) {
        global $DB;

        $data = (object)$data;

        $data->voteid = $this->get_new_parentid('vote');
        $data->userid = $this->get_mappingid('user', $data->userid);
        $data->optionid = $this->get_mappingid('option', $data->optionid);

        $newitemid = $DB->insert_record('vote_votes', $data);
    }

    protected function after_execute() {
    }
}