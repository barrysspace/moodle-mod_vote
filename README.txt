ABOUT
==========
The 'Vote' module was developed by
    Neill Magill

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The Vote module allows you to create a simple selection of options for which students will vote.

INSTALLATION
==========
The Vote module follows the standard installation procedure.

1. Create folder <path to your moodle dir>/mod/vote.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.
