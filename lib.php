<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module vote
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the vote specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// The types of vote.
define('VOTE_TYPE_POLL', 1);
define('VOTE_TYPE_VOTE', 2);
define('VOTE_TYPE_AV', 3);

// The states a vote can be in.
define('VOTE_STATE_EDITING', 0);
define('VOTE_STATE_ACTIVE', 1);

// Functions that can be passed by parameter to the vote.
define('VOTE_FUNC_QUESTION', 1);
define('VOTE_FUNC_QUESTION_DELETE', 2);
define('VOTE_FUNC_OPTION', 3);
define('VOTE_FUNC_OPTION_DELETE', 4);
define('VOTE_FUNC_RESET', 5);
define('VOTE_FUNC_VOTE', 6);
define('VOTE_FUNC_ACTIVATE', 7);

define('VOTE_CACHE_TIME', 60); // The cache will be valid for 1 minute.

// Moodle core API.

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function vote_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:
        case FEATURE_COMPLETION_TRACKS_VIEWS:
        case FEATURE_COMPLETION_HAS_RULES:
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
        case FEATURE_GRADE_OUTCOMES:
        case FEATURE_RATE:
        case FEATURE_COMMENT:
        case FEATURE_GROUPINGS:
        case FEATURE_GROUPS:
            return false;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the vote into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $vote An object from the form in mod_form.php
 * @param mod_vote_mod_form $mform
 * @return int The id of the newly inserted vote record
 */
function vote_add_instance(stdClass $vote, mod_vote_mod_form $mform = null) {
    global $DB;

    $vote->timecreated = time();

    return $DB->insert_record('vote', $vote);
}

/**
 * Updates an instance of the vote in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $vote An object from the form in mod_form.php
 * @param mod_vote_mod_form $mform
 * @return boolean Success/Fail
 */
function vote_update_instance(stdClass $vote, mod_vote_mod_form $mform = null) {
    global $DB;

    $vote->timemodified = time();
    $vote->id = $vote->instance;

    return $DB->update_record('vote', $vote);
}

/**
 * Removes an instance of the vote from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function vote_delete_instance($id) {
    global $DB;

    if (! $vote = $DB->get_record('vote', array('id' => $id))) {
        return false;
    }

    $DB->delete_records('vote_result_cache', array('voteid' => $vote->id));
    $DB->delete_records('vote_votes', array('voteid' => $vote->id));
    $DB->delete_records('vote_options', array('voteid' => $vote->id));
    $DB->delete_records('vote_question', array('voteid' => $vote->id));

    $DB->delete_records('vote', array('id' => $vote->id));

    return true;
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in vote activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 */
function vote_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;  //  True if anything was printed, otherwise false.
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link vote_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function vote_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@see vote_get_recent_mod_activity()}
 *
 * @return void
 */
function vote_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * @return boolean
 **/
function vote_cron () {
    return true;
}

/**
 * Returns an array of users who are participanting in this vote
 *
 * Must return an array of users who are participants for a given instance
 * of vote. Must include every user involved in the instance,
 * independient of his role (student, teacher, admin...). The returned
 * objects must contain at least id property.
 * See other modules as example.
 *
 * @param int $voteid ID of an instance of this module
 * @return boolean|array false if no participants, array of objects otherwise
 */
function vote_get_participants($voteid) {
    global $DB;

    $params = array();
    $params['voteid'] = $voteid;
    $sql = "SELECT DISTINCT u.id FROM {role_assignments} r "
            ."JOIN {user} u ON u.id = r.userid "
            ."LEFT JOIN {vote_votes} v ON u.id = v.userid "
            ."WHERE v.voteid = :voteid";

    return $DB->get_records_sql($sql, $params);
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function vote_get_extra_capabilities() {
    return array('mod/vote:view',
        'mod/vote:submit',
        'mod/vote:edit');
}

// File API.

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function vote_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * Serves the files from the vote file areas
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return void this should never return to the caller
 */
function vote_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload) {
    global $DB, $CFG;

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);

    send_file_not_found();
}

/**
 * Obtains the automatic completion state for this vote based on any conditions
 * in vote settings.
 *
 * @global moodel_database $DB
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function vote_get_completion_state($course, $cm, $userid, $type) {
    global $DB;

    // Get the vote's details.
    if (!($vote = $DB->get_record('vote', array('id' => $cm->instance)))) {
        throw new Exception("Can't find vote {$cm->instance}");
    }

    $result = $type; // Default return value.

    if (!empty($vote->completionvoted)) { // Users must have voted.
        $voted = $DB->record_exists('vote_votes', array('voteid' => $vote->id, 'userid' => $userid));

        if ($type == COMPLETION_AND) {
            $result &= $voted;
        } else {
            $result |= $voted;
        }
    }

    return $result;
}

/**
 * Generate content for the course overview.
 *
 * A message will be displayed if:
 * - The user can vote.
 * - The user has not voted.
 * - The vote closes in the next 7 days.
 *
 * @param mixed $courses The list of courses to print the overview for
 * @param array $htmlarray The array of html to modified.
 * @return void
 */
function vote_print_overview($courses, &$htmlarray) {
    global $PAGE;

    // Check that courses have been passed.
    if (empty($courses) || !is_array($courses) || count($courses) == 0) {
        return;
    }

    // Get all the vote activities in the courses, if none present do nothing more.
    if (!$votes = get_all_instances_in_courses('vote', $courses)) {
        return;
    }

    // Get the vote renderer.
    $renderer = $PAGE->get_renderer('mod_vote');
    $oneweekinfuture = time() + 604800;

    foreach ($votes as $vote) {
        if ($vote->votestate != VOTE_STATE_ACTIVE || $vote->closedate > $oneweekinfuture) {
            // The vote is not active, or closes in more than the warning time.
            continue;
        }

        $renderablevote = new mod_vote_renderable($vote);
        if (!$renderablevote->can_submit() || $renderablevote->has_voted()) {
            // The user cannot vote, or has already voted.
            continue;
        }

        // If we get here the user can vote, but has not so let us tell them about the vote.
        if (!array_key_exists($vote->course, $htmlarray)) {
            $htmlarray[$vote->course] = array();
        }
        if (!array_key_exists('vote', $htmlarray[$vote->course])) {
            $htmlarray[$vote->course]['vote'] = ''; // initialize, avoid warnings
        }
        $htmlarray[$vote->course]['vote'] .= $renderer->render_overview($renderablevote);
    }
}
