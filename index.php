<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is a one-line short description of the file
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_vote
 * @copyright  2012 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = required_param('id', PARAM_INT);   // Course.

$course = get_course($id);

require_course_login($course);

$eventdata = array(
    'context' => $vote->id,
    'objectid' => $absenceform->id,
);

$event = \mod_vote\event\allvotes_viewed::create($eventdata);
$event->trigger();

$coursecontext = context_course::instance($course->id);

$PAGE->set_url('/mod/vote/index.php', array('id' => $id));
$PAGE->set_title(format_string($course->fullname));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($coursecontext);

echo $OUTPUT->header();

if (! $votes = get_all_instances_in_course('vote', $course)) {
    notice(get_string('novotes', 'vote'), new moodle_url('/course/view.php', array('id' => $course->id)));
}

$table = new html_table();

$usesections = course_format_uses_sections($course->format);

if ($usesections) {
    $table->head  = array(get_string('sectionname', 'format_' . $course->format), get_string('name'), get_string('description'));
    $table->align = array('center', 'left');
} else {
    $table->head  = array(get_string('name'), get_string('description'));
    $table->align = array('left', 'left', 'left');
}

foreach ($votes as $vote) {
    if (!$vote->visible) {
        $link = html_writer::link(
            new moodle_url('/mod/vote.php', array('id' => $vote->coursemodule)),
            format_string($vote->name, true),
            array('class' => 'dimmed'));
    } else {
        $link = html_writer::link(
            new moodle_url('/mod/vote.php', array('id' => $vote->coursemodule)),
            format_string($vote->name, true));
    }

    $description = html_writer::tag('div', $vote->intro, array('class' => 'no-overflow'));

    if ($usesections) {
        $table->data[] = array(get_section_name($course, $vote->section), $link, $description);
    } else {
        $table->data[] = array($link, $description);
    }
}

echo $OUTPUT->heading(get_string('modulenameplural', 'vote'), 2);
echo html_writer::table($table);
echo $OUTPUT->footer();
